function buildSettingsFile(s, settingsFilename)
%  build reconstruction settings file

if ~exist('s', 'var')
   s = struct;
end

   function val = qgf(fieldName, default)
      [val, s] = utils.queryGetField(s, fieldName, default);
   end


switch nargin
   case 0
      
      %% CHANGE SETTINGS HERE %%
      % uncomment and edit the settings you wish to change
      % vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
      
      % s.doIndepIter = true;
      % boolean
      % Decide whether or not to run frame-by-frame independent
      % reconstruction
      
      % s.doUseGpu = true;
      % boolean
      % Decide whether or not to compute on GPU
      
      % s.dataDir = '../data';
      % char vector
      % Input and output data file directory, this directory should contain
      % 2 directories 'psf_files' and '02_rectified'
      
      % s.psfFilename = 'psf_01';
      % char vector
      % PSF matrix file for reconstruction
      
      % s.inputFilename = 'cylinder_801_m125/im_2.tif';
      % char vector
      % Input data file to be reconstructed, must be a .tif or .mat file
      
      % s.whichSolver = 1;
      % Deconvolution method. Current version supports only Richardson-Lucy
      % Richardson-Lucy Deconvolution: 1
      
      % s.maxIter = 8;
      % integer, x >= 1
      % Number of iteration per each frame. Large number of iteration
      % results in higher resolution/contrast at the price of computation
      % time and pronounced artifacts
      
      % s.firstFrameIndex = 1;
      % integer, x >= 1
      % If the data is a time series in .mat format, user can decide the
      % range for reconstruction as [FirstFrame:DecimationRatio:LastFrame]
      
      % s.lastFrameIndex = 1;
      % integer, x >= firstFramInd
      % If the data is a time series in .mat format, user can decide the
      % range for reconstruction as [FirstFrame:DecimationRatio:LastFrame]
      
      % s.frameIncrement = 1;
      % integer, x >= 1
      % If the data is a time series in .mat format, user can decide the
      % range for reconstruction as [FirstFrame:DecimationRatio:LastFrame]
      
      % s.doSaturate = false;
      % boolean
      % Output will be automatically normalized to the full scale. User can
      % decide to "amplify" the results for better visualization
      
      % s.saturationGain = 1.5;
      % number
      % Output will be automatically normalized to the full scale. User can
      % decide to "amplify" the results for better visualization
      
      % s.contrast = 0.95;
      % number, x > 0
      % Contrast adjustment value used if one wants to use the result from
      % last frame as the initial guess for the next frame. contrast<1 is
      % often required to avoid artifact. Low value will result in low
      % contrast in the result.
      
      % s.doEdgeSuppress = false;
      % boolean
      % Since border area in the reconstruction result is often subject to
      % artifacts, user can simply assign zeros to the region by turning
      % this option on.
      
      % s.doUseDiskVariable = false;
      % boolean
      % Result can be directly saved the result on disk in a frame-by-frame
      % manner. Recommended for large data. SSD is strongly recommended.
      
      % settingsFilename = 'unamed_settings.mat';
      
      % ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      
   case 1
   case 2     
   otherwise
      error(strcat('Invalid number of arguments, 0, 1, or 2 expected', ...
         ' but %d were passed'), nargin);
end

% sets defaults for all parameters that have not yet been set
qgf('doIndepIter', true);
qgf('doUseGpu', true);
qgf('dataDir', '../data');
qgf('psfFilename', 'psf_01');
qgf('inputFilename', '171201_high_data/im_1.tif');
qgf('whichSolver', 1);
qgf('maxIter', 8);
qgf('firstFrameIndex', 1);
qgf('lastFrameIndex', 1);
qgf('frameIncrement', 1);
qgf('doSaturate', false);
qgf('saturationGain', 1);
qgf('contrast', 1);
qgf('doEdgeSuppress', false);
qgf('doUseDiskVariable', false);

if ~exist('settingsFilename', 'var')
   settingsFilename = 'unamed_settings';
end

%% SAVING SETTINGS FILE - DO NOT EDIT %%

scriptDir = fileparts(mfilename('fullpath'));
oldwd = pwd();
cd(scriptDir);
settingsDir = fullfile(scriptDir, 'settings_files');
if ~isdir(settingsDir)
   mkdir(settingsDir);
end
fullpath = fullfile(settingsDir, strcat(settingsFilename, '.mat'));
save(fullpath, '-struct', 's');
cd(oldwd);
end
