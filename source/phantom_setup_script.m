%% Input
fprintf('Loading Input ...\n');
phantomType = 'beadCorkscrew';
imSize = [801, 801];
margin = 125;

%%% Dependent Input
fName = sprintf('%s_%d_%d_101_m%d', phantomType, imSize(1), ...
   imSize(2), margin) ;
zFactor = 8;
zPsfSize = 101;
TIF_EXT = '.tif';

%% Load PSF
fprintf('Loading PSF ...\n');
if exist('PSF','var')
   warning('PSF already loaded into memory; will not load again.');
else
   disp('Preparing to load PSF, press any key to continue ...');
   pause;
   PSF = load('../data/psf_files/psf_01.mat');
end

%% Make Phantom
fprintf('Making Phantom ...\n');
zExpandedSize = (zFactor * (zPsfSize - 1)) + 1;
volumeSize = [imSize, zExpandedSize];
phantom = buildPhantomVolume(phantomType, volumeSize, margin);
phantom_8Bit = uint8(round(255 * phantom));
fPath = fullfile('../data/phantoms', strcat(fName, '_full_1', TIF_EXT));
utils.volume2tif(phantom_8Bit, fPath);

phantomSlice = phantom(:,:,1:zFactor:end);
phantomSlice_8Bit = uint8(round(255 * phantomSlice));
fPath = fullfile('../data/phantoms', strcat(fName, '_1', TIF_EXT));
utils.volume2tif(phantomSlice_8Bit, fPath);

%% Create LF Image
fprintf('Building light-field image from phantom ...\n');
zeroArrGPU = gpuArray.zeros(ceil((imSize + 181 - 1) / 128) * 128);
lfim = solver.volume2lfimGPU(phantomSlice, PSF.H, zeroArrGPU);
lfim = gather(lfim);

lfim_8bit = uint8(round(lfim / max(lfim(:)) * 255));
mkdir('../data/02_rectified', fName);
fPath = fullfile('../data/02_rectified', fName, strcat('im_1', TIF_EXT));
imwrite(lfim_8bit, fPath);

%% Rotation
fprintf('Rotating Phantom for side view image ...\n');
phantom = shiftdim(phantom, 2);
%phantom_8Bit = uint8(round(255 * phantom));
%fPath = fullfile('../data/phantoms', strcat(fName, '_full_2', TIF_EXT));
%utils.volume2tif(phantom_8Bit, fPath);

phantomSlice = phantom(:,:,1:zFactor:end);
%phantomSlice_8Bit = uint8(round(255 * phantomSlice));
%fPath = fullfile('../data/phantoms', strcat(fName, '_2', TIF_EXT));
%utils.volume2tif(phantomSlice_8Bit, fPath);

%% Nudge Phantom 2 out of alignment

%% Rotation LF Image
fprintf('Building side view light field image ...\n');
zeroArrGPU = gpuArray.zeros(ceil((imSize + 181 - 1) / 128) * 128);
lfim = solver.volume2lfimGPU(phantomSlice, PSF.H, zeroArrGPU);
lfim = gather(lfim);

lfim_8bit = uint8(round(lfim / max(lfim(:)) * 255));
fPath = fullfile('../data/02_rectified', fName, strcat('im_2', TIF_EXT));
imwrite(lfim_8bit, fPath);

%% Settings Filse
fprintf('Making settings files ...\n');
settings.saturationGain = 2;
settings.inputFilename = fullfile(fName, strcat('im_1', TIF_EXT));
buildSettingsFile(settings, strcat(fName, '_1'));
settings.inputFilename = fullfile(fName, strcat('im_2', TIF_EXT));
buildSettingsFile(settings, strcat(fName, '_2'));

%% Reconstruction
fprintf('Performing Reconstructions ...\n\n');
reconstruct(strcat(fName, '_1'), PSF);
reconstruct(strcat(fName, '_2'), PSF);


%% Phantom Reconstruction Loading
fprintf('\n\nLoading phantom reconstructions ...\n');
fPath = fullfile('../data/03_reconstructed', fName, ...
   'recon_3D_im_1.tif');
imInfo = imfinfo(fPath);
nFrames = length(imInfo);
imSize = [imInfo(1).Height, imInfo(1).Width];
% TODO - make im2volume function
p1 = zeros([imSize, nFrames]);
for iFrame = 1:nFrames
   p1(:,:,iFrame) = imread(fPath, 'Index', iFrame);
end

fPath = fullfile('../data/03_reconstructed', fName, ...
   'recon_3D_im_2.tif');
imInfo = imfinfo(fPath);
nFrames = length(imInfo);
imSize = [imInfo(1).Height, imInfo(1).Width];
p2 = zeros([imSize, nFrames]);
for iFrame = 1:nFrames
   p2(:,:,iFrame) = imread(fPath, 'Index', iFrame);
end

%% Interpolate
fprintf('Interpolating z-dimension of phantom reconstructions ...\n');
p1Full = utils.arrayInterp(p1, 3, 8);
p2Full = utils.arrayInterp(p2, 3, 8);

%% Registration



%% Overlay Volumes To Different Color Chanels
fprintf('Placing both volumes into color tiff stacks ...\n');
p2Full_rotate_back = shiftdim(p2Full, 1);
zeroVol = zeros(size(p1Full));

volStack = cell(3, 1);
volStack{1} = uint8(round(p1Full));
volStack{2} = uint8(round(p2Full_rotate_back));
volStack{3} = uint8(round(zeroVol));

fPath = fullfile('../data/03_reconstructed', fName, ...
   'recon_3D_im_color_register.tif');
utils.volume2tif(volStack, fPath);
fprintf('Done!\n');
