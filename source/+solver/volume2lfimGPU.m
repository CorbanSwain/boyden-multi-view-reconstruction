function I = volume2lfimGPU(volume, psf, zeroArrGPU)
% realspace volume to light field image, forwardProjection

[nPsfRows, nPsfCols, nLensletRows, nLensletCols, nLayers] = size(psf);
[nVolumeRows, nVolumeCols, ~] = size(volume);

[I, zeroImage] = ...
   deal(gpuArray(zeros([nVolumeRows, nVolumeCols], 'single')));

psfSliceSelection = cell(1, 5);
psfSliceSelection{1} = 1:nPsfRows;
psfSliceSelection{2} = 1:nPsfCols;

METHOD = 1;

switch METHOD
   case 1
      psfSliceSelection = cell(1, 5);
      psfSliceSelection{1} = 1:nPsfRows;
      psfSliceSelection{2} = 1:nPsfCols;
      
      for iLensletRow = 1:nLensletRows
         psfSliceSelection{3} = iLensletRow;
         
         for iLensletCol = 1:nLensletCols
            psfSliceSelection{4} = iLensletCol;
            lensletPixelSelection = {
               iLensletRow:nLensletRows:nVolumeRows, ...
               iLensletCol:nLensletCols:nVolumeCols};
            
            for iLayer = 1:nLayers
               psfSliceSelection{5} = iLayer;
               psfSlice = gpuArray(squeeze(psf(psfSliceSelection{:})));
               
               singleLensletPixelImage = zeroImage;
               singleLensletPixelImage(lensletPixelSelection{:}) = ...
                  volume(lensletPixelSelection{:}, iLayer);
               
               projectedIm = utils.conv2fft(singleLensletPixelImage, ...
                  psfSlice, zeroArrGPU);
               I = I + projectedIm;
            end % for iLayer
            
         end % for iLensletCol
         
      end % for iLensletRow
      
   case 2
      psfSliceSelection = cell(1, 5);
      psfSliceSelection{1} = 1:nPsfRows;
      psfSliceSelection{2} = 1:nPsfCols;
      psfSliceSelection{5} = 1:nLayers;
      
      for iLensletRow = 1:nLensletRows
         psfSliceSelection{3} = iLensletRow;
         
         for iLensletCol = 1:nLensletCols
            psfSliceSelection{4} = iLensletCol;
            
            psfStack = gpuArray(squeeze(psf(:, :, iLensletRow, ...
               iLensletCol, :)));
            psfStack = squeeze(num2cell(psfStack, [1, 2]));
            
            lensletPixelSelection = {
               iLensletRow:nLensletRows:nVolumeRows, ...
               iLensletCol:nLensletCols:nVolumeCols};
            singleLensletPixelImageStack = cell(nLayers, 1);
            [singleLensletPixelImageStack{:}] = deal(zeroImage);
            for iLayer = 1:nLayers
               singleLensletPixelImageStack{iLayer}(...
                  lensletPixelSelection{:}) = volume(...
                  lensletPixelSelection{:}, iLayer);
            end
            
            s = struct('psf', psfStack, 'im', ...
               singleLensletPixelImageStack);
            
            fun = @(a) utils.conv2fft(a.im, a.psf, zeroArrGPU);
            A = arrayfun(fun, s, 'UniformOutput', false);
            I = I + sum(cat(3, A{:}), 3);
            
         end % for iLensletCol
         
      end % for iLensletRow
      
   case 3
      
      for iLensletRow = 1:nLensletRows
   
            psfStack = gpuArray(squeeze(psf(:, :, iLensletRow, ...
               :, :)));
            psfStack = reshape(psfStack, nPsfRows, nPsfCols,...
               nLensletCols * nLayers);
            psfStack = squeeze(num2cell(psfStack, [1, 2]));
            
            lensletPixelSelection = cell(2, 1);
            lensletPixelSelection{1} = ...
               iLensletRow:nLensletRows:nVolumeRows;
            singleLensletPixelImageStack = cell(nLayers * nLensletCols, 1);
            [singleLensletPixelImageStack{:}] = deal(zeroImage);
            pos = 0;
            for iLensletCol = 1:nLensletCols
               %FIXME - use ind2sub here
               lensletPixelSelection{2} = ... 
                  iLensletCol:nLensletCols:nVolumeCols;
               for iLayer = 1:nLayers
                  pos = pos + 1;
                  singleLensletPixelImageStack{pos}(...
                     lensletPixelSelection{:}) = volume(...
                     lensletPixelSelection{:}, iLayer);
               end
            end
            
            s = struct('psf', psfStack, 'im', ...
               singleLensletPixelImageStack);
            
            fun = @(a) utils.conv2fft(a.im, a.psf, zeroArrGPU);
            A = arrayfun(fun, s, 'UniformOutput', false);
            I = I + sum(cat(3, A{:}), 3);
            
         
      end % for iLensletRow
end

I = cast(I, 'double');


