function volume = lfim2volumeGPU(I, psfFlip, zeroArrGPU)
% light field image to realspace volume, backProjection

[nPsfRows, nPsfCols, nLensletRows, nLensletCols, nLayers] = size(psfFlip);
[nImageRows, nImageCols] = size(I);

volume = gpuArray(zeros([nImageRows, nImageCols, nLayers], 'single'));
zeroSlice = volume(:,:,1);

psfSliceSelection = cell(1, 5);
psfSliceSelection{1} = 1:nPsfRows;
psfSliceSelection{2} = 1:nPsfCols;

for iLayer = 1:nLayers
   volumeSlice = zeroSlice;
   psfSliceSelection{5} = iLayer;
   
   for iLensletRow = 1:nLensletRows
      psfSliceSelection{3} = iLensletRow;
      
      for iLensletCol = 1:nLensletCols
         psfSliceSelection{4} = iLensletCol;
         psfSlice = gpuArray(squeeze(psfFlip(psfSliceSelection{:})));
         
         lensletPixelSelection = {
            iLensletRow:nLensletRows:nImageRows, ...
            iLensletCol:nLensletCols:nImageCols};
         
         singleLensletPixelImage = zeroSlice;
         singleLensletPixelImage(lensletPixelSelection{:}) = ...
            I(lensletPixelSelection{:});
         
         volumeSlice = volumeSlice ...
            + utils.conv2fft(singleLensletPixelImage, psfSlice, ...
            zeroArrGPU);
      end % for iLensletCol
      
   end % for iLensletRor
   
   volume(:,:,iLayer) = volumeSlice;
end % for iLayer
