function guessVolume = rldeconvGPU(startImage, guessVolume, psf, ...
   psfFlip, nIterations, zeroArrGPU)
% richardson-lucy deconvolution implementation for volume reconstruction of 
% rectified light field images.

i2v = @(I) solver.lfim2volumeGPU(I, psfFlip, zeroArrGPU);
v2i = @(volume) solver.volume2lfimGPU(volume, psf, zeroArrGPU);

statusFmt = '    Iteration %02d/%02d took %6.1f seconds ...\n';

if guessVolume == false
   tic;
   startImageVolume = i2v(startImage);
   fprintf(statusFmt, 0, nIterations, toc);
    
   guessVolume = startImageVolume;
else  % guessVolume already assigned
   error('Unexpected number of arguments in rldeconvGPU.');
end

calcVersion = 2;
for iIteration = 1:nIterations
   tic;
   guessImage = v2i(guessVolume);
   
   switch calcVersion
      case 1 % Corban Version
         diffImage = startImage ./ guessImage;
         
         % Cleaning up any non-numerical values
         diffImage(isnan(diffImage)) = 0;
         diffImage(isinf(diffImage)) = 1;
         
         guessVolume = guessVolume .* i2v(diffImage);
         
      case 2 % Original Version
         if ~exist('startImageVolume', 'var')
            tic;
            startImageVolume = i2v(startImage);
            fprintf(statusFmt, 0, nIterations, toc);
         end
         
         guessImageVolume = i2v(guessImage);
         errorVolume = startImageVolume ./ guessImageVolume;
         
         % Cleaning up any non-numerical values
         errorVolume(isnan(errorVolume)) = 0;
         errorVolume(isinf(errorVolume)) = 1;
         
         guessVolume = guessVolume .* errorVolume;
         
      otherwise
         error('Invalid calcVersion in rldeconv.');
   end
   
   fprintf(statusFmt, iIteration, nIterations, toc);
end