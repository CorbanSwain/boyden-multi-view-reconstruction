function reconstruct(varargin)
% Lightfield (LF) image reconstruction command line tool
% CorbanSwain January 2018
% Based on reconstruction software from Prevedel et al, 2014

fprintf('Starting light-field reconstruction script at\n  %s ...\n\n', ...
   datetime('now'));

%% INPUT HANDLING %
didLoadPsf = false;
switch nargin
   case 0
      settingsFilename = 'defaults';
      
   case 1
      settingsFilename = varargin{1};
      
   case 2
      settingsFilename = varargin{1};
      PSF = varargin{2};
      didLoadPsf = true;
      
   otherwise
      error('Unexpected number of arguments passed to reconstruct().');
end


%% SETUP
% move to this script's directory
oldwd = pwd();

% Running as function
sourceDir = fileparts(mfilename('fullpath'));

% Boyden Lab PC
% sourceDir = ['C:\Users\user1\Documents\corban_swain\repos\', ...
%              'boyden_rotation\ReconstructScript'];

% RIVERTREE PC
% sourceDir = ['C:\Users\CorbanSwain\Repos\boyden_lab', ...
%              '\multi_view_reconstruction\source'];
          
% UNDERSTANDING Mac
% sourceDir = ['/Users/CorbanSwain/Repos/boyden_lab/', ...
%              'multi_view_reconstruction/source'];

cd(sourceDir);

% load Settings
settingsFullPath = ['settings_files' filesep settingsFilename];
fprintf('Loading settings from settings file ''%s''.\n\n', ...
   settingsFilename);
settings = load(settingsFullPath);

% constants
OUTPUT_CLASS = 'uint8'; % type for output volume
PSF_DIR_NAME = 'psf_files';
RECTIFIED_DIR_NAME = '02_rectified';
RECONSTRUCT_DIR_NAME = '03_reconstructed';
TIF_EXT = '.tif';
MAT_EXT = '.mat';
errorIdFmt = sprintf('%s:%s', mfilename, '%s'); % format for error message

% satGain: relative maximum value of the brightest part of the first frame
% if the value is greater than 1, parts of the first frame will ve clipped
% to white. Value must be positive, 1 by default.
if settings.doSaturate
   satGain = settings.saturationGain;
else
   satGain = 1;
end

% identifiers

% check for input data directory
if ~isdir(settings.dataDir)
   es.identifier = sprintf(errorIdFmt, 'MissingDataDir');
   es.message = sprintf('''%s'' is not a directory.', settings.dataDir);
   error(es);
end

% get absolute path of data directory
cd(settings.dataDir);
dataDir = pwd;
cd(sourceDir);

% data subdirectories
fullDataPath = @(s) fullfile(dataDir, s);
rectifiedDir = fullDataPath(RECTIFIED_DIR_NAME);
reconstructedDir = fullDataPath(RECONSTRUCT_DIR_NAME);

tempFmt = sprintf('''%s'' directory not found in ''%s''.', '%s', ...
   settings.dataDir);

% check for data subdirectories
if ~didLoadPsf
   psfDir = fullDataPath(PSF_DIR_NAME);
   if ~isdir(psfDir)
      es.identifier = sprintf(errorIdFmt, 'MissingPSFDir');
      es.message = sprintf(tempFmt, PSF_DIR_NAME);
      error(es);
   end
end
if ~isdir(rectifiedDir)
   es.identifier = sprintf(errorIdFmt, 'MissingRectifiedDir');
   es.message = sprintf(tempFmt, RECTIFIED_DIR_NAME);
   error(es);
end

% validate input file type
[inputDir, inputName, inputExt] = fileparts(settings.inputFilename);
isValidInput = strcmp(inputExt, TIF_EXT) || strcmp(inputExt, MAT_EXT);
if ~isValidInput
   es.identifier = sprintf(errorIdFmt, 'InvalidFileType');
   es.message = 'Input must be a ''.tif'' or ''.mat'' file.';
   error(es);
end

% make output directories
if ~isdir(reconstructedDir)
   mkdir(reconstructedDir);
end
saveDir = fullfile(reconstructedDir, inputDir);
if ~isdir(saveDir)
   mkdir(saveDir);
end



%% LOAD DATA
% load PSF file
if ~didLoadPsf
   psfPath = fullfile(psfDir, settings.psfFilename);
   fprintf('Loading PSF, ''%s'', this takes some time ...\n', ...
      settings.psfFilename);
   try
      if exist('PSF','var')
         error('PSF already loaded into memory; will not load again.');
      end
      disp('Loading PSF in 15 seconds ...');
      pause(15);
      PSF = load(psfPath);
   catch exception
      if strcmp(exception.identifier, 'MATLAB:array:SizeLimitExceeded')
         msgtext = strcat('PSF file is too large for memory, at', ...
            ' least 32.0 GB of memory is recommended.');
         id = sprintf(errorIdFmt, 'InsuffucuentMemory');
         ME = MException(id, msgtext);
         exception = addCause(exception, ME);
      end
      rethrow(exception);
   end
   fprintf('Done.\n');
end 

% storing dimension information
psfSize = size(PSF.H);
psfImageSize = psfSize(1:2);
psfVolumeSize = psfSize([1, 2, 5]);
psfSourceSize = psfSize(3:4);
fprintf('PSF matrix has size [%s].\n\n', num2str(psfSize));

% cast PSF matrices to have single precision
PSF.H = cast(PSF.H, 'single');
PSF.Ht = cast(PSF.Ht, 'single');

% load input images
inputPath = fullfile(rectifiedDir, settings.inputFilename);
fprintf('Loading input from file ''%s'' ...\n', ...
   settings.inputFilename);
switch inputExt
   case TIF_EXT
      inputImages = im2double(imread(inputPath));
   case MAT_EXT
      load(inputPath, 'inputImages');
end
fprintf('Done.\n');

% storing dimension information
imageSize = [size(inputImages, 1), size(inputImages, 2)];
nFrames = size(inputImages, 3);
fprintf('Image has size [%s] with %d frame(s).\n\n', ...
   num2str(imageSize), nFrames);

% define size of the reconstructed volume
volumeSize = [imageSize, psfVolumeSize(3)]; % GLOBAL

% validate settings
errId = sprintf(errorIdFmt, 'InvalidSetting');
errStr = strcat('''lastFrameIndex'' setting must be an integer', ...
   ' less than the number of frames in the image');
assert(settings.lastFrameIndex <= nFrames, errId, errStr);



%% DEFINE PROJECTION FUNCTIONS
if settings.doUseGpu
   % calculate the minimum extended image size by adding half of the PSF
   % image width and heigth to the image width and heigth, respectively
   minPadImageSize = imageSize + floor(psfImageSize / 2);
   
   % expand the padded dimensions to either a power of two or a multiple
   % of 128, choosing whichever is smaller. This to speed up the FFT
   % calculations used for convolution.
   padImageSize = min(2 .^ ceil(log2(minPadImageSize)), ...
      128 * ceil(minPadImageSize / 128));
   fprintf('FFT image size is calculated to be [%s].\n\n', ...
      num2str(padImageSize));
   
   % create GPU array of zeros for performing projections for
   % reconstructin
   zeroArrGPU = gpuArray(zeros(padImageSize));
   
   % define back and forward projection functions
   switch settings.whichSolver
      case 1
         buildVolume = @(I) solver.rldeconvGPU(I, false,...
            PSF.H, PSF.Ht, settings.maxIter, zeroArrGPU);
         
         % for the dependenr reconstruction only one iteration is used
         buildDependentVolume = @(image, guessVolume) ...
            solver.rldeconvGPU(image, guessVolume, PSF.H, PSF.Ht, ...
            1, zeroArrGPU);
         
      otherwise
         es.identifier = sprintf(errorIdFmt, 'NotImplemented');
         es.message = strcat('Only Richardson-Lucy deconvolution', ...
            ' is avalible; set ''whichSolver'' to 1 in the', ...
            ' settings file to use that method.');
         error(es);
   end
else
   % TODO - add CPU computation case
   es.identifier = sprintf(errorIdFmt, 'NotImplemented');
   es.message = strcat('CPU Computation is not yet implemented,', ...
      ' set ''doUseGPU'' to ''true'' in the', ...
      ' settings file.');
   error(es);
end



%% RUN RECONSTRUCTION
% subsample image slices based on frame increment
processFrameIndices = colon(settings.firstFrameIndex, ...
   settings.frameIncrement, ...
   settings.lastFrameIndex);
nProcessFrames = length(processFrameIndices);
processImages = inputImages(:, :, processFrameIndices);

if settings.doUseDiskVariable
   % TODO - add disk variable computation implementation
   es.identifier = sprintf(errorIdFmt, 'NotImplemented');
   es.message = strcat('Disk cacheing is not yet implemented,', ...
      ' set ''doUseDiskVariable'' to ''false'' in', ...
      ' the settings file.');
   error(es);
else
   outputVolumeSequence = zeros([volumeSize, nProcessFrames], ...
      OUTPUT_CLASS);
end
fprintf('Beginning volume reconstruction ...\n');

if settings.doEdgeSuppress
   selectionCenter = false(volumeSize -  (2 * [psfSourceSize, 0]));
   edgeSelection = padarray(selectionCenter, psfSourceSize, true, 'both');
end

% loop through frames
for iFrame = 1:nProcessFrames
   fprintf('  Frame %03d/%03d:\n', iFrame, nProcessFrames);
   
   % check if on first frame
   isFirstFrame = (iFrame == 1);
   
   % extract image to work on, cast to single
   I = cast(processImages(:, :, iFrame), 'single');
   
   if isFirstFrame || settings.doIndepIter
      % reconstruct the volume from the image using the chosen method
      outputVolume = buildVolume(I);
   else
      % reconstruct the volume using the previous volume as a starting
      % point with some contrast adjustment as specified in the settings
      % file.
      adjustedOutputVolume = utils.contrastAdjust(outputVolume, ...
         settings.contrast);
      outputVolume = buildDependentVolume(I, adjustedOutputVolume);
   end
   
   if isFirstFrame
      % determine the value to adjust the gain of the output volume
      gainAdjustment = gather(satGain / max(outputVolume(:)));
   end
   
   % bring outputVolume back to the CPU
   if settings.doUseGpu
      outputVolume = gather(outputVolume);
   end
   
   % post-processing: adjust gain and rescale to output precision
   % FIXME - not all possible output classes have an intmax() value this
   % will cause an error (e.g. if OUTPUT_CLASS = 'double'). Maybe deal with
   % this by using try-catch.
   outMax = double(intmax(OUTPUT_CLASS));
   outputVolume = cast(round(outMax * gainAdjustment ...
      * outputVolume), OUTPUT_CLASS);
   
   if settings.doEdgeSuppress
      outputVolume(edgeSelection) = 0;
   end
   
   if settings.doUseDiskVariable
      % Not implemented
   else
      % add the outputVolume to the ouput sequence
      fprintf('    Storing volume from frame %d in memory ...\n', iFrame);
      outputVolumeSequence(:, :, :, iFrame) = outputVolume;
      fprintf('    Done!\n');
   end
   
end % for iFrame

fprintf('All frames complete.\n\n');



%% SAVE RECONSTRUCTED VOLUMES

fprintf('Saving reconstructed volume(s) ...\n');

% build output file path
baseName = 'recon_3D_';
if settings.doSaturate
   baseName = strcat(baseName, 'sat_');
end
outputName = strcat(baseName, inputName);
outputPath = fullfile(saveDir, outputName);

% FIXME - this will only process one frame as is
iFrame = 1;
switch inputExt
   case TIF_EXT      
      utils.volume2tif(outputVolumeSequence(:, :, :, iFrame), ...
         outputPath);
      
   case MAT_EXT
      if settings.doUseDiskVariable
         % not implemented
         
         % shouldn't have to do much here since the data is already saved
         
      else
         outputPath = strcat(outputPath, MAT_EXT);
         save(outputPath, 'outputVolumeSequence', '-v7.3');
      end
end

fprintf('Done.\n\nReconstruction script finished at\n  %s.\n\n\n', ...
   datetime('now'));

cd(oldwd);