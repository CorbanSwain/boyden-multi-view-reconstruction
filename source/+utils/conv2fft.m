function Y = conv2fft(X, M, padArr)
% 2-D convolution by using multiplication in the frequency domain 

sizeX = size(X);
sizeM = size(M);

switch nargin
   case 2
      padArr = zeros(sizeX + sizeM - 1);
      
   case 3
      
   otherwise
      error('conv2fft:InvalidNumberArguments', strcat('conv2fft can ',...
         'only accept 2 or 3 arguments; %d were passed.', nargin));
end

[xPad, mPad] = deal(padArr);

% FIXME - need additional padding style options
xPad(1:sizeX(1), 1:sizeX(2)) = X;
mPad(1:sizeM(1), 1:sizeM(2)) = M;

X = xPad;
M = mPad;

M = circshift(M, -floor(sizeM / 2));

Y = real(ifft2(fft2(X) .* fft2(M)));
Y = Y(1:sizeX(1), 1:sizeX(2));