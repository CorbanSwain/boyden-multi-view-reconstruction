function volume = buildPhantomVolume(volumeId, volumeSize, margin)
% creates a volume with a specific phantom to test reconstruction scripts
%
% INPUT:
% volumeId: identifier for the type of volume to create
% volumeSize: vector of length 3 specifying thr final dimensions of the
% volume
% margin: a scalr representing the number of voxels to pad the phantom with
% on all sides

% FIXME - validate inputs

% compute the size of the central region containing the phantom
phantomSize = volumeSize -  (2 * margin);

dimVectors = cell(3, 1);
[dimVectors{:}] = zeroCenterVector(phantomSize([2, 1, 3]));
[Y, X, Z] = meshgrid(dimVectors{:});

switch volumeId
   case 'sphere'
      sphereRadius = min(phantomSize) / 2;
      rsquared = X .^ 2 + Y .^ 2 + Z .^2;
      phantom = rsquared < (sphereRadius ^ 2);
      
   case 'cylinderShell'
      thicknessRatio = 0.1;
      cylinderRadius = min(phantomSize(1:2)) / 2;
      xyrsquared = X .^ 2 + Y .^ 2;
      innerRadius = cylinderRadius - (cylinderRadius * thicknessRatio);
      phantom = (xyrsquared < (cylinderRadius ^ 2)) ...
         & (xyrsquared > (innerRadius ^ 2));
      
   case '100beads'
      nBeads = 100;
      radiusRatio = 0.01;
      
      beadRadius = min(phantomSize) * radiusRatio;
      sphereCenterSize = phantomSize(:) - (2 * beadRadius);
      % size of the dimensional space that can contain sphere centers
      % also ensure column vector
      beadLocs = rand(3, nBeads) .* (sphereCenterSize  - 1) ...
         - ((sphereCenterSize - 1) / 2);
      
      rsqAt = @(loc) (X - loc(1)) .^ 2 + (Y - loc(2)) .^ 2 ...
         + (Z - loc(3)) .^2;
      phantom = false(phantomSize);
      for iBead = 1:nBeads
         phantom = phantom | (rsqAt(beadLocs(:,iBead)) < beadRadius ^ 2);
      end
      
   case '10beads'
      nBeads = 10;
      radiusRatio = 0.08;
      
      beadRadius = min(phantomSize) * radiusRatio;
      sphereCenterSize = phantomSize(:) - (2 * beadRadius);
      % size of the dimensional space that can contain sphere centers
      % also ensure column vector
      beadLocs = rand(3, nBeads) .* (sphereCenterSize  - 1) ...
         - ((sphereCenterSize - 1) / 2);
      rsqAt = @(loc) (X - loc(1)) .^ 2 + (Y - loc(2)) .^ 2 ...
         + (Z - loc(3)) .^2;
      phantom = false(phantomSize);
      for iBead = 1:nBeads
         phantom = phantom | (rsqAt(beadLocs(:,iBead)) < beadRadius ^ 2);
      end
      
   case 'beadGimbal'
      beadsPerCircle = 16;
      radiusRatio = 0.04;
      
      nBeads = beadsPerCircle * 3;
      beadRadius = min(phantomSize) * radiusRatio;
      circRadius = (min(phantomSize) / 2) - beadRadius;
      
      t = linspace(0, 1, beadsPerCircle + 1);
      t = t(1:(end - 1));
      t = t(:); % ensure column vector
      circparam = circRadius * [cos(t * 2 * pi()), sin(t * 2 * pi())];
      beadLocs = zeros(3, nBeads);
      currentSelection = 1:beadsPerCircle;
      beadLocs([1, 2], currentSelection) = circparam';
      currentSelection = currentSelection + beadsPerCircle;
      % FIXME - use rotation matrix for these two steps
      beadLocs([2, 3], currentSelection) = circparam';
      currentSelection = currentSelection + beadsPerCircle;
      beadLocs([3, 1], currentSelection) = circparam';
      
      % rotate bead location so gimbal is tilted
      % R = roty(45) * rotx(45);
      % beadLocs = R * beadLocs;
      
      rsqAt = @(loc) (X - loc(1)) .^ 2 + (Y - loc(2)) .^ 2 ...
         + (Z - loc(3)) .^2;
      phantom = false(phantomSize);
      for iBead = 1:nBeads
         phantom = phantom | (rsqAt(beadLocs(:,iBead)) < beadRadius ^ 2);
      end
      
   case 'beadCorkscrew'
      beadsPerCircle = 32;
      radiusRatio = 0.02;
      nTurns = 8;
      
      nBeads = beadsPerCircle * nTurns;
      beadRadius = min(phantomSize) * radiusRatio;
      circRadius = (min(phantomSize(1:2)) / 2) - beadRadius;
      
      t = linspace(0, nTurns, nBeads + 1);
      t = t(1:(end - 1));
      zLoc = linspace(-1, 1, nBeads) * ((phantomSize(3) / 2) - beadRadius);
      
      beadLocs =  [cos(t * 2 * pi()) * circRadius
         sin(t * 2 * pi()) * circRadius
         zLoc];
      
      % rotate bead location so gimbal is tilted
      % R = roty(45) * rotx(45);
      % beadLocs = R * beadLocs;
      
      rsqAt = @(loc) (X - loc(1)) .^ 2 + (Y - loc(2)) .^ 2 ...
         + (Z - loc(3)) .^2;
      
      METHOD = 2;
      
      switch METHOD
         case 1
            phantom = false(phantomSize);
            fprintf('Bead: ');
            for iBead = 1:nBeads
               fprintf('%3d/%3d ', iBead, nBeads);
               phantom = phantom | (rsqAt(beadLocs(:,iBead)) ...
                  < beadRadius ^ 2);
               fprintf('\b\b\b\b\b\b\b\b');
            end
            fprintf('Done!\n');
            
         case 2  % Alternate method:
            % TODO - functionalize this method
            % make sphere at origin
            rtest = ceil(beadRadius);
            vecTest = (-rtest):rtest;
            [Xtest, Ytest, Ztest] = ndgrid(vecTest, vecTest, vecTest);
            originSphere = (Xtest .^ 2) + (Ytest .^ 2) + (Ztest .^ 2) ...
               < (beadRadius ^ 2);
            originSphereVector = originSphere(:);
            % [I, J, K] = ind2sub(phantomSize, 1:prod(phantomSize));
            originSpherePoints = [Xtest(originSphereVector)'
               Ytest(originSphereVector)'
               Ztest(originSphereVector)'];
            originSpherePoints_4D = zeros(4, size(originSpherePoints, 2));
            originSpherePoints_4D(1:3,:) = originSpherePoints;
            originSpherePoints_4D(4,:) = 1; % * here assign original points
            
            % create 4D transition matrices
            beadLocsInd = beadLocs + ((phantomSize(:) + 1) / 2);
            T = repmat(eye(4), 1, 1, nBeads);
            T(1:3, 4, :) = reshape(beadLocsInd(:), [3, 1, nBeads]);
            
            % 'OR' all of the locations together into the final volume
            phantom = false(phantomSize);
            fprintf('Bead: ');
            % FIXME - use pagefun
            for iBead = 1:nBeads
               newSpherePoints_4D = round(T(:,:,iBead) ...
                  * originSpherePoints_4D);
               fprintf('%3d/%3d ', iBead, nBeads);
               
               % check values are within the valid dimensions
               isValid = (newSpherePoints_4D >= 1) ...
                  & (newSpherePoints_4D <= [phantomSize(:); 1]);
               isValid = all(isValid, 1);
               goodPoints = newSpherePoints_4D(:,isValid);
               
               selection = sub2ind(phantomSize, ...
                  goodPoints(1,:), ...
                  goodPoints(2,:), ...
                  goodPoints(3,:));
               phantom(selection) = true; % * here usre the 4th row
               fprintf('\b\b\b\b\b\b\b\b');
            end
            fprintf('Done!\n');
      end
      
   otherwise
      error('No phantom volume ID matching ''%s'' avalible. ', volumeId);
      
end

volume = padarray(phantom, margin .* ones(3,1), 'both');

function varargout = zeroCenterVector(vecLengths)
nVectors = length(vecLengths);
varargout = cell(nVectors, 1);
for iVectors = 1:nVectors
   n = vecLengths(iVectors);
   varargout{iVectors} = (1:n) - (n + 1) / 2;
end