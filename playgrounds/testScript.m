clear;
I = imread('cameraman.tif');
I = cast(I, 'double') / 255;
I = I(:,:);
imshow(I);
r = 2; % radius in pixels

sz = size(I);
nele = prod(sz);
domain{2} = (1:sz(1)) - ((1 + sz(1)) / 2);
domain{1} = (1:sz(2)) - ((1 + sz(2)) / 2);
[X, Y] = meshgrid(domain{:});
cir = sqrt(X .^ 2 + Y .^ 2) < r;

psf = false(sz);
steps = 50;
param = linspace(0, 1, steps)';
traj = [cos(param * pi / 2), sin(param * pi / 2)] * 10;
traj = round(traj);

doParallel = false;
processFunc = @(shift) circshift(cir, shift);
if doParallel
    linPsf = psf(:);
    
    T = zeros(3, 3, steps); % Transition Matrices
    T(1, 1, :) = 1;
    T(2, 2, :) = 1;
    T(3, 3, :) = 1;
    T(1, 3, :) = traj(:,1);
    T(2, 3, :) = traj(:,2);
    
    % Convert image points to locations and value pairs
    [C(1,:), C(2,:,1), C(3,:)] = ind2sub(sz, 1:nele);
    
    for i = 1:steps
        fprintf('Step %02d ... ', i);
        C_star = T(:,:,i) * C;
        fprintf('A ... ');
        C_star = C_star(:, (C_star(1,:) >= 1) & (C_star(1,:) <= sz(1)));
        fprintf('B ... ');
        C_star = C_star(:, (C_star(2,:) >= 1) & (C_star(2,:) <= sz(2)));
        fprintf('C ... ');
        inds = sub2ind(sz, C_star(1,:), C_star(2,:));
        linPsf(inds) = linPsf(inds) | cir(inds);
        clear('inds');
        fprintf('Done!\n');
    end
else
    for i = 1:steps
        psf = psf | processFunc(traj(i,:));
    end
end

imshow(psf);

psf = psf / sum(psf(:)); % normalize to sum of 1
PSF1 = circshift(psf, floor(sz / 2));
PSF2 = circshift(psf, floor(sz / 2) + 1);

conv1 = real(ifft2(fft2(I) .* fft2(psf)));
imshow(conv1);

conv2 = real(ifft2(fft2(I) .* fft2(PSF1)));
imshow(conv2);

conv3 = real(ifft2(fft2(I) .* fft2(PSF2)));
imshow(conv3);

%%
% Richardson-Lucy Deconvolution

convolvefft2 = @(A,B) real(ifft2(fft2(A) .* fft2(B)));

flippedPsf =flip(flip(psf,1),2);

% prep psf matrices for using in the convolvefft2 function
psfShift = circshift(psf, floor(sz / 2) + 1);
flippedPsfShift = circshift(flippedPsf, floor(sz / 2) + 1);

observedIm = conv3;

initialGuess = convolvefft2(observedIm, flippedPsfShift);
imGuess = initialGuess;

nIterations = 5;
figure;

for iIter = 1:nIterations
    imGuess = imGuess .* initialGuess ...
        ./ convolvefft2(imGuess, psfShift);
    fprintf('Round: %d', iIter);
    subplot(1,5,iIter);
    imshow(imGuess);
end