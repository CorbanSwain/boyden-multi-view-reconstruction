%%%%%%%%%%%%%%% Generate Synthetic Volume %%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Nneurons = 5350;
wholeVolume = zeros(volumeResolutionQ);
numVoxels = prod(volumeResolution);
[XX YY ZZ] = meshgrid(1:volumeResolutionQ(1), 1:volumeResolutionQ(2), (1:volumeResolutionQ(3)));
centroidNeuron_GT = zeros(Nneurons,3);

for i=1:Nneurons,
   
   xyzLoc = 0.9*volumeResolutionQ .* rand(1,3) + 0.05*volumeResolutionQ;
   xyzLoc = 0.2*volumeResolutionQ .* (rand(1,3) - 0.0) + 0.55*volumeResolutionQ;
   centroidNeuron_GT(i,:) = xyzLoc;
   radius = 2;
   radius = 1;
   brightness = 4 + 0.1*rand(1);
   
   neuronLoc = (sqrt((xyzLoc(2)-XX).^2+(xyzLoc(1)-YY).^2+ (xyzLoc(3)-ZZ).^2) < radius);
   
   neuronLoc = ceil(numVoxels*rand(1,1));
   wholeVolume(neuronLoc) = 1;
end
save(['wholeVolume5.mat'], 'Nneurons', 'wholeVolume', 'volumeResolutionQ', 'radius',  '-v7.3');