This file contains basic instructions for installing and running the multi-view Lucy Richardson deconvolution package for multi-GPU architectures. The code provided here has been tested with the 64-bit version of Windows 7 and with the 64-bit version of Ubuntu Linux 12.04 LTS, using a variety of CUDA-compatible NVIDIA GPUs.


1. CONTENTS OF THE SOFTWARE ARCHIVE
-----------------------------------
We assume that the user uncompressed the zip file in a folder of their choice, referred to here as $ROOT_MVLR_GPU. The subfolders in $ROOT_MVLR_GPU contain the following software components and data:

-"bin": Windows 7 64-bit executables for running the code. 
-"src": All source code files. 
-"test": A cropped IsoView test data set (anterior region of a Drosophila first instar larva expressing GCaMP6s panneuronally), consisting of four registered views with corresponding PSFs.

The root folder contains the file CMakeList.txt, which can be used to compile the source code on any platform.


2. INSTALLATION AND SOFTWARE REQUIREMENTS 
-----------------------------------------
In order to run the precompiled binaries, the following auxiliary software package must be installed as well:

-CUDA Toolkit 5.5 or above (Windows binaries are compiled with version 6.5): This software is required to run code on an NVIDIA GPU
 Download link: https://developer.nvidia.com/cuda-toolkit-archive
 For Ubuntu Linux distributions, you can also execute the following command in a terminal: sudo apt-get install nvidia-cuda-toolkit

Note: The code relies on the cuFFT library. This library contains bugs in version 7.0 (even when installing the patch released by NVIDIA). Please do not compile the code using version 7.0 as it can return NaN entries in the output data.

The source code can be compiled for any operating system using CMake.


3. SETTING UP THE XML CONFIGURATION FILE
----------------------------------------
In order to pass file names and parameters to the multi-view deconvolution software you need to prepare an XML file following the structure of our example file ($ROOT_MVLR_GPU\bin\config.xml). The XML configuration file contains an entry <view> for each view, which lists the image file location, PSF file location and the affine transformation needed to register the respective view. A separate entry <deconvolution> specifies the parameters for multi-view deconvolution. In the following, we provide a quick overview of the required tags and attributes:

* View[XML tag]

One �view� entry is required per image. The information in this tag informs the software about the location of image and point spread function (PSF) files and about the affine transformation needed to register the image data with the other views.

A[attribute]: 16 floating point numbers defining the 3D affine transformation (4x4 matrix) that needs to be applied to the image in order to register it to the other images.

imgFilename[attribute]: Full path name of the image file for this view (in KLB format).

psfFilename[attribute]: Full path name of the PSF file for this view (in KLB format). The PSF data file is typically much smaller than the primary image data file.

* Deconvolution[XML tag]

Only one �deconvolution� entry is required. It specifies the parameters for the Lucy-Richardson deconvolution algorithm.

imBackground[attribute]: Floating point scalar defining the background intensity level of the image. This value is subtracted from all images before deconvolution.

lambdaTV[attribute]: Floating point scalar indicating the weight for total variation (TV) regularization. If this parameter is set below or equal to zero, total variation regularization is not applied during deconvolution.

numIter[attribute]: Integer indicating the number of iterations for which the Lucy-Richardson algorithm is executed.

verbose[attribute]: Integer indicating whether the program should output intermediate results for debugging purposes. If the parameter is set to zero, no intermediate image is output. Otherwise, PSFs, primary images and contrast weights are output after applying the affine transformation. If registration information is correctly provided, overlap between different views after affine transformation should be optimal.

blockZsize[attribute]: Integer indicating whether image data should be partitioned in blocks in the Z dimension in order to allow processing of very large images. Only the executable main_multiviewDeconvLR_multiGPU_blocksZ.exe uses this attribute for processing very large images. It is recommended to set this parameter to a power of 2 or 3 (or a combination of these factors) for best efficiency.


4. RUNNING THE SOFTWARE
-----------------------
We provide an IsoView image data set that allows the user to test the code and familiarize themselves with the software configuration before applying the code to their own data sets. We note that input image stacks need to be provided in KLB format, which ensures that different regions of interest of the stacks can be efficiently accessed without overhead. Code and documentation for the KLB format can be found here: https://bitbucket.org/fernandoamat/keller-lab-block-filetype.

In order to run the code on the test data set, please execute the following steps:

1.-Open a terminal.
2.-Go to the folder that contains the executable multiview_deconvolution_LR_multiGPU.exe (which is found in the "bin" folder described above).
3.-Execute the following command:
   multiview_deconvolution_LR_multiGPU.exe $XML_File_Full_Path 

Note: The placeholder $XML_File_Full_Path needs to be replaced by the full path name of the XML configuration file accompanying the test data set. Please make sure to update the path names provided in the XML file before running the software. Also, for successful execution of the software, please make sure to place the XML file in a folder without blank spaces in the folder name.

