#!/bin/sh
#SBATCH --ouput.out
#SBATCH -c 5
#SBATCH --ntask=1
#SBATCH --mem-per-cpu 15000
#SBATCH --time=05-00:00:00
#SBATCH --array=14-300%75
#SBATCH --exclude node017,node018
#SBATCH --mail-type=ALL
#SBATCH --mail-user=c_swain@mit.edu

# // your code goes here//
